<?php

/*
 * Functions for the SAAO website theme.
 *
 * IMPORTANT: Some of the code applies to Gravity Forms forms and fields, and the id
 * values in the code must be consistent with the form and field ids of the targeted
 * forms and fields.
 */

// MAKING THIS THEME'S STYLES AVAILABLE

/*
 * Enqueue this theme's stylesheet.
 */
function saao_enqueue_style() {
    wp_enqueue_style('saao-child-style', get_stylesheet_uri());
}

add_action('wp_enqueue_scripts', 'saao_enqueue_style');

// LOGGING IN AND OUT

/*
 * Add a logout link to the navigation menu.
 *
 * Adapted from https://wpbeaches.com/adding-loginlogout-existing-menu-wordpress/.
 */
function saao_logout_menu_link($items, $args) {
    if (is_user_logged_in()) {
        if ($args->theme_location == 'top-navigation') {
            $items .= '<li class="menu-item menu-item-type-post_type menu-item-object-page loginout" >'
                . wp_loginout('index.php', false)
                . '</li>';
        }
    }
    return $items;
}

add_filter('wp_nav_menu_items', 'saao_logout_menu_link', 10, 2);

/*
 * Shortcode for a login/logout link.
 */
function saao_loginout_shortcode($atts) {
    // Sanitize user input
    $a = shortcode_atts(array(
        'redirect_to' => 'index.php'
    ), $atts);
    $redirect_to = esc_url($a['redirect_to']);

    // Create the link
    return wp_loginout($redirect_to, false);
}

add_shortcode('saao_loginout', 'saao_loginout_shortcode');

// DON'T SHOW ADMIN CONTENT TO SUBSCRIBERS

/*
 * Don't show the admin bar to subscribers.
 */
function saao_remove_admin_bar() {
    $user = wp_get_current_user();
    $show_admin_bar = false;
    if ( isset( $user->roles ) && is_array( $user->roles )) {
        if ( in_array( 'administrator', $user->roles ) || in_array( 'editor', $user->roles ) || in_array( 'author', $user->roles ) || in_array( 'contributor', $user->roles ) ) {
            $show_admin_bar = true;
        }
        if ( is_admin() ) {
            $show_admin_bar = true;
        }
    }
    show_admin_bar($show_admin_bar);
}

add_action('after_setup_theme', 'saao_remove_admin_bar');


// SAAO PUBLICATIONS

/*
 * Shortcode for returning the latest 100 SAAO publications.
 */
function saao_publications_shortcode($atts)
{
    // get the number of publications to display
    $a = shortcode_atts(array(
        'count' => '100'
    ), $atts);
    $count = intval($a['count']);  // intval returns 0 for invalid input
    if ($count <= 0) {
        return "Oops. Is the value of the count attribute invalid? It must be a positive integer.";
    }

    // get the query URL for the Web of Science API
    // see https://developer.clarivate.com/apis/woslite for the API documentation
    $base_url = "https://wos-api.clarivate.com/api/woslite";
    $params = [
        'databaseId' => 'WOS',
        'usrQuery' => 'OG="South African Astronomical Observatory"',
        'sortField' => 'PY+D',
        'firstRecord' => 1,
        'count' => $count
    ];
    $url = $base_url . "?" . http_build_query($params);

    // configure the API request
    $ch = curl_init($url);
    $api_key_header = 'X-ApiKey: ' . get_option('saao_wos_api_key');
    curl_setopt($ch, CURLOPT_HTTPHEADER,
        [$api_key_header]
    );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    try {
        // make the API request
        $result = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($result)->Data;

        // create an unordered list with the publications
        $output = '<ul>';
        foreach ($data as $row) {
            $output .= '<li>'
                . _saao_publications_authors_string($row->Author)
                . ': '
                . '&quot;' . _saao_publications_title_string($row->Title) . '&quot;'
                . ', '
                . _saao_publications_source_string($row->Source)
                . ' '
                . _saao_publications_link_string($row->Other)
                . '</li>';
        }
        $output .= '</ul>';
        return $output;
    } catch (Exception $e) {
        return 'Oops. The publications list could not be retrieved.';
    }
}

/**
 * The string to use for the authors of a publication.
 *
 * @param $author object Author object
 * @return string
 */
function _saao_publications_authors_string($author) {
    try {
        $authors = $author->Authors;
        if (count($authors) > 3) {
            return $authors[0] . ', ' . $authors[1] . ', ' . $authors[2] . ', et al.';
        }
        else {
            return join(', ', $authors);
        }
    }
    catch (Exception $e) {
        return '?';
    }

}

/**
 * The string to use for the title of a publication.
 *
 * @param $title object Title object
 * @return mixed|string
 */
function _saao_publications_title_string($title) {
    try {
        return $title->Title[0];
    }
    catch (Exception $e) {
        return '?';
    }
}

/**
 * The string to use for a publication source. This generally includes the journal, volume, pages abd publication year.
 * If the publication is part ofd a book or series, the book or series is included in the string.
 *
 * @param $source object Source object
 * @return string
 */
function _saao_publications_source_string($source) {
    try {
        // collect all available details
        // as the various pieces of information are all separated by comma, we don't have to keep track of what pieces
        //we have and we can collect them in an array
        $parts = [];

        // publication year
        $year = $source->{'Published.BiblioYear'}[0];
        if ($year) {
            $parts[] = $year;
        }

        // journal
        // common journal names are abbreviated
        $journals = [
            "ASTRONOMICAL JOURNAL" => "AJ",
            "ASTRONOMY & ASTROPHYSICS" => "A&amp;A",
            "ASTROPHYSICAL JOURNAL" => "ApJ",
            "ASTROPHYSICAL JOURNAL LETTERS" => "ApJL",
            "MONTHLY NOTICES OF THE ROYAL ASTRONOMICAL SOCIETY" => "MNRAS"
        ];
        $journal = $source->SourceTitle[0];
        if (array_key_exists($journal, $journals)) {
            $journal = $journals[$journal];
        }

        // book or series
        $series = $source->BookSeriesTitle[0];

        // combine journal and series
        if ($journal || $series) {
            if ($journal) {
                $parts[] = $journal . ($series ? ' (' . $series . ')' : '');
            } else {
                $parts[] = $series;
            }
        }

        // volume
        $volume = $source->Volume[0];
        if ($volume) {
            $parts[] = $volume;
        }

        // pages
        $pages = $source->Pages[0];
        if ($pages) {
            $parts[] = $pages;
        }

        // return the pieces as a comma-separated list
        return join(', ', $parts);
    }
    catch (Exception $e) {
        return '?';
    }
}

/**
 * A string for accessing the ADS page fore a publication.
 *
 * @param $other object Other object
 * @return string
 */
function _saao_publications_link_string($other) {
    try {
        $doi = $other->{"Identifier.Doi"}[0];
        if ($doi) {
            return '[<a href="http://adsabs.harvard.edu/doi/' . $doi . '">VIEW</a>]';
        }
    }
    catch (Exception $e) {
        return '';
    }

    // no DOI found
    return '';
}

/**
 * Add the shortcode for accessing 100 SAAO publications.
 *
 * The publications are sorted by publication year (in descending order), but it is not guaranteed that they are
 * sorted by month within a publication year.
 *
 * Shortcode usage:
 *
 * [saao_publications]
 */
add_shortcode( 'saao_publications', 'saao_publications_shortcode' );


/*
 * Add a SAAO publications submenu page to the settings menu.
 */
function saao_register_saao_publications_options_page() {
    add_submenu_page(
        'options-general.php',
        'SAAO Publications',
        'SAAO Publications',
        'manage_options',
        'saao-publications-options',
        '_saao_publications_options_page' );
}

/*
 * The SAAO publications settings page.
 *
 * Adapted from https://travis.media/where-do-i-store-an-api-key-in-wordpress/.
 */
function _saao_publications_options_page() {
    $wos_api_key = get_option("saao_wos_api_key", "");
    ?>
    <div class="wrap"><div id="icon-tools" class="icon32"></div>
        <h2>SAAO Publications</h2>
        <form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST">
            <h3>Web of Science API Key</h3>
            <input type="text" name="wos_api_key" value="<?php echo($wos_api_key); ?>" placeholder="Enter API Key">
            <input type="hidden" name="action" value="saao_publications_process_form">
            <input type="submit" name="submit" id="submit" class="update-button button button-primary" value="Save API Key"  />
        </form>
    </div>
    <?php
}

/*
 * Store the Web of Science API key.
 */
function submit_saao_wos_api_key() {
    if (isset($_POST['wos_api_key'])) {
        $api_key = sanitize_text_field( $_POST['wos_api_key'] );
        $api_exists = get_option('saao_wos_api_key');
        update_option('saao_wos_api_key', $api_key);
    }
    wp_redirect($_SERVER['HTTP_REFERER']);
}

add_action( 'admin_post_nopriv_saao_publications_process_form', 'submit_saao_wos_api_key' );
add_action( 'admin_post_saao_publications_process_form', 'submit_saao_wos_api_key' );

/**
 * Add the SAAO publications settings page.
 */
add_action('admin_menu', 'saao_register_saao_publications_options_page');


// USER REGISTRATION

/*
 * A function for changing the default "Other" text on Gravity Forms radio button
 * fields. The new text is "I'd rather specify myself".
 *
 * This function only updates fields in the form with the id 2.
 */
function set_user_registration_other_value( $placeholder, $field ) {
    if ( 3 === $field->formId ){ // 3 is the form id number.
        $placeholder = "I'd rather specify myself";
    }
    return $placeholder;
}

/*
 * Add the filter for changing the default "Other" text on Gravity Forms radio button
 * fields.
 */
add_filter( 'gform_other_choice_value', 'set_user_registration_other_value', 10, 2 );

/*
 * Validate Gravity Forms field input, ensuring that it is a four-digit year between
 * 1900 and the current year.
 */
function validate_user_registration_phd_year( $result, $value, $form, $field ) {

    if ( $result["is_valid"] ) {
        if ( !preg_match("/^\d{4}$/", $value) ) {
            $result["is_valid"] = false;
            $result["message"] = "The year must be a four-digit number.";
            return $result;
        }
        $year = intval(sanitize_text_field($value));
        if ( $year < 1900 ) {
            $result["is_valid"] = false;
            $result["message"] = "The year must 1900 or later.";
        }
        $currentYear = intval(date("Y"));
        if ($year > $currentYear) {
            $result["is_valid"] = false;
            $result["message"] = "The year must not be in the future.";
        }
    }
    return $result;
}

/*
 * Add the filter for validating a year.
 *
 * The filter is applied to the field with id 16 in the form with id 2.
 */
add_filter( 'gform_field_validation_3_16', 'validate_user_registration_phd_year', 10, 4 );

// ACCEPTING A PROPOSAL

/*
 * Shortcode for displaying the form for accepting proposals only if the user is logged
 * in.
 */
function saao_accept_proposal_shortcode() {
    if (!is_user_logged_in()) {
        return "<div>"
            . "<p>You must be logged in to accept a proposal.</p>"
            . "<p>" . wp_loginout('/astronomers/accept-a-proposal', false) . "</p>"
            . "<p><a href=\"/register-as-an-saao-user/\">Register</a></p>"
            . "</div>";
    }

    return do_shortcode('[gravityform id="4" title="false" description="false" ajax="true"]');
}

add_shortcode('saao_accept_proposal', 'saao_accept_proposal_shortcode');
