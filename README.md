# SAAO Website Theme

A WordPress child theme for the SAAO website.

## Prerequisites

The Bridge WordPress theme must be installed before this child theme can be installed.

## Installation

The easiest way to install (and update) the theme is to use the [WP Pusher](https://wppusher.com) plugin, which is free as long as the theme repository is public.

Alternatively, you can first clone the repository

```shell
git clone git@bitbucket.org:saaoit/saao-theme.git
```

and then zip the created directory without its hidden files,

```shell
zip -r saao-website-theme.zip saao-website-theme -x "*/.*"
```

This zip file can then be uploaded as a WordPress theme (cf. [https://www.wpbeginner.com/beginners-guide/how-to-install-a-wordpress-theme/](https://www.wpbeginner.com/beginners-guide/how-to-install-a-wordpress-theme/)).

## SAAO publications

The theme defines a shortcode `[saao_publications]` for displaying a list of SAAO publications. Before you can use the shortcode, you need to set an API key for the Web of Science. This is done on the SAAO Publications settings page, which can be found under the Settings menu on the dashboard.

The shortcode takes the following attributes:

Attribute | Optional? | Default | Description
--- | --- | --- | ---
count | Yes | 100 | The (maximum) number of publications to display. The latest publications are shown.

For example, if you want to display 100 publications, you can use:

```
[saao_publications]
```

If you only require 50 publications, you can use:

```
[saao_publications count="50"]
```

## Logging in and out

The theme adds a logout link to the top navigation menu if the user is logged in.

It also adds a `saao_loginout` shortcode, which creates a a login/logout link.

The shortcode takes the following attributes:

Attribute | Optional? | Default | Description
--- | --- | --- | ---
redirect_to | Yes | index.php | URL to which the user should be redirected after logging in or out.

For example, if you want to display a login/logout button redirecting to the home page, you can use something like:

```
<span class="button button-loginout">
    [saao_loginout]
</span>
```

The classes for the span tag are examples only; use whatever classes are appropriate for your use case.

If the button should redirect to a page `/astronomers`, you can use:

```
<span class="button button-loginout">
    [saao_loginout redirect_to="/astronomers"]
<span>
```

You cannot redirect to a page on another server.

The theme hides the admin bar at the top of the page from users who aren't administrator, editor, contributor or author.

## Caveats

The `functions.php` file contains some Grabity Forms filters which target specific form and field ids. You thus cannot use the file on other websites without modification.

The Web of Science API key is stored in the WordPress database without being encrypted.

# Acknowledgements

https://travis.media/where-do-i-store-an-api-key-in-wordpress/
